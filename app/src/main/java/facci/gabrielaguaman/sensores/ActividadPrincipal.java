package facci.gabrielaguaman.sensores;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadPrincipal extends AppCompatActivity {

    Button btnSensor1, btnSensor2, btnSensor3, btnVibrador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        btnSensor1 = (Button) findViewById(R.id.btnSensor1);
        btnSensor1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });

        btnSensor2 = (Button) findViewById(R.id.btnSensor2);
        btnSensor2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, ActividadSensorBrujula.class);
                startActivity(intent);
            }
        });

        btnSensor3 = (Button) findViewById(R.id.btnSensor3);
        btnSensor3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, ActividadSensorAproximidad.class);
                startActivity(intent);
            }
        });

        btnVibrador = (Button) findViewById(R.id.btnVibrador);
        btnVibrador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadPrincipal.this, ActividadVibrador.class);
                startActivity(intent);
            }
        });
    }
}
